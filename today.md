# Meeting Notes

- Combine phone and extension into one field
- Add password field on landing form
- Put photo graph in edit page and remove from register page
- Yes no for check mark instead of checkbox
- Are you willling to drive ?
- Will you do grocery shopping ?
- Switch to languages instead of language ?
- Toggle switch to ask for willing to cook and clean ?
- description for skills on hover
- Refine skills list with description
- N/A for drivers licence
- cange distance to slider
- add top margin on info page
- Calculate rate with taxes and duration

## After MVP:

- negotiation through messaing ? add field in visit create for rate
- PSW Check list before and after check-in check out
- Report abuse, block person
- Send messages to multiple caregivers at once
- compare caregivers conmponent


# Visits

## Visit Grid
- Get only data belongs to that user (query events for the loggedInUser) `4h` [Iegor]
- Test / Fix check in / check out functionalty `2h` [Iegor]
- Send Message to right user converstaion thread `2h` [Hemanth]
- Paginate visits add load more buttton, remember pagination when switching pages `1d` [Iegor]
- Question: How to do Rescheduling (cancel and create new or edit existing) ?

## Visit New / Edit
- Test and fix create new visit form `4h` [Hemanth]
- Get rate from user schema for Estimate amount for a visit   `2h` [Hemanth]

## Messages Modules
- Get Only data belonging to loggedInUser `2h` [Hemanth]
- Paginate and store data for subsequent requests `4h` [Hemanth]
- Testing creating new visit from messages `2h` [Hemanth]
- Paginate conversation thread `4h` [Hemanth]
- Show Calendar invitation card `2h` [Hemanth]

## User Gridlist (caregiver / clients)
- Paginate and store results `1d` [Iegor]
- Show photo on the cargiver card `4h` [Iegor]

## User Edit
- Change save icon to floppy instead of checkmark `1h` [Hemanth]

## Caregiver search
- User al-multi-checklist instead of al-checkbox for skills field `2h` [Iegor]
- Implement mobiscroll range slider `2h` [Iegor]
- Use languages array instead of language `2h` [Iegor]

## User Details `2h` [Iegor]
- Hide phone number, email, locaiton
- Show city for location
- Add rate to header card instead of tab content
- Tab1:  Add missing fields Gender, work permit, vulenrable sector, PSW Certification, CPR certification
- Tab2: Language

## Profile
- Add profile picture on top header, Move context menu to top header instead of secondary header `1d` [Hemanth]
- Add edit profile button `2h` [Hemanth]
