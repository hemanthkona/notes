
Notifications: 

 - 1 trigger can trigger multiple actions based on the conditions 

Email: 

  AWS SES :  
    - can send 62,000 free emails per month  
    - $0.10 per 1,000 email = $2 for 20,000 emails after the free tier  

  Mandrill :  
    - $20 per 25000 emails or less 

SMS: 
   AWS SNS :
     - First 1 million push notifications are free per month 
     - $1 per 1 million push notifications after free tier 

- Trigger has multiple conditions
- based on the Conditions take actions 

Predefined notifications: 

  - [x] Send notification on creating new event to caregiver 
     - Email 10 minites (action) 
     - Email 
  - [x] Send 

Custom or User Defined notifications: 

 condition: (attribute) event status -> (opereator) is equal to (value) in-progress, or is scheduled  
Use Cases 1: 
 action: 

 condition: Blood Pressure is greater than 80
 action: Push notification immediately  (Present)
 action: Email after 1 day (condition)on missed-check-out automatically changed to completed (Past)
 action: Email before 30 minutes  (Future)

(Event)Trigger: {
  description: string,
  conditions or (filters) : [{
    attribute: string || id, event_status,
    operator: id | string, is equal to, greater than,
    value: Integer || id
  }],
  actions: [{
    type: id, ('send sms | send push message | send email')
    time: 10,
    time_unit: id, ( minutes, days ),
    time_tense: past, present, future
    template: 'Write customer message with {{variables}}' 
  },
  {
    type: id, 
    before_time: 1,
    before_time_unit: id, ( minutes, days )
  }],
  sender (producer): id || { },  Opengears, webhooks, heart-rate monitoring devices 
  reciever (consumer): id || { } 
}


Trigger->create()


- Triggers are the rules 
- Its goog idea to save all notification to the database.
- Notification is the actual message, datetime notification is sent 
- When a notification is sent, it saves to database before sending to end user

Questions: 

- How to schedule notifications for the future. 

Notification: {
  message: string 
  trigger: id,
  to: id,
  from: id,
  type: push | sms | email | pusher,
  sent: boolean,
  received: boolean,
  resend: boolean  
  _created_date: 12/23/2018
}

User: {
  triggers : {
    'trigger_id': true,
    'trigger_id': false
  }
}

User has toggled: Send notification on create event, before 1 day
  - event create controller 
  - calls Trigger::event_created (event) {
    - get event.attendee triggers 
    - 
    - 
  }

Send missed-check-in notification: 
  -  
 
Attributes event.status = 'created'

- How we are going to loop through and schedule or send notification ?
- How the email notifications are scheduled in the backend currently ?
- How do we manage triggers for individual person ?
- How do we manage custom triggers created by user ?
- How do we pick up or check for triggers ?  

Example: 

Trigger : 

  - Send email notification on  

Conditions: 

  - Create new event
  - Missed-check-in event 
  - Missed-check-out event
  - 

Actions: 

  - Send 

Tuts: 
  ☐ https://medium.com/aws-activate-startup-blog/a-guide-to-amazon-simple-notification-service-mobile-push-self-registration-for-ios-f85d114d42b8#.5p8ebpj2t
  ☐ https://medium.com/@emzosmizo/designing-notifications-196bc52e6f0a#.sc4vt790x
  ☐ https://medium.com/@intercom/designing-smart-notifications-36336b9c58fb#.runwzcy40

Schema Design: 

  - https://www.mongodb.com/blog/post/schema-design-for-social-inboxes-in-mongodb

Existing solutions: 
  
  - https://getstream.io
  -  


Questions: 

  - How we are going to schedule notifications ?
    - Can we use AWS to schedule notifications in the future ?
  - 

Todo: 

  - Aws SNS actions 
    - Send SMS Message to a number 
      - How to send message to one person phone number instead of sending to a list of subscribers ? 
      - How to track whether the user STOPED receiving sms notifications 

    - Send Email notification to an email 
      - Switch to production mode to remove the limitations: http://docs.aws.amazon.com/ses/latest/DeveloperGuide/request-production-access.html
      - 
    - Send push notification to a mobile device platform
      - 

AWS SNS: 

  Topics: 
  Subscribers: 
  Publishsers: 


- Platform application for creating mobile app notifications  

- Create corodva app 
- Move dist folder to www folder 
- Add Andorid platform 
- add push notification plugin 
- build project 
- 

Blocked: 
 - Google cloud messaging credentials 


Instructions for Push Notifications 
- Create a project in Firebase Cloud Messaging; https://console.firebase.google.com
-  Get server key from from https://console.firebase.google.com/project/bloom-test-74d53/settings/cloudmessaging use it when creating application platform in the AWS Console 
- Get SENDER_ID from : https://console.firebase.google.com/project/bloom-test-74d53/settings/cloudmessaging and use it when adding phonegap push notifications plugin 
- Create a application platform in AWS Console: https://console.aws.amazon.com/sns/v2/home?region=us-east-1#/applications

# Creating mobile app using cordova
- Install cordova: sudo npm install -g cordova
- Create cordova project: cordova create bloom com.bloom.app BloomApp 
- cd in bloom directory 
- Add android platform:  cordova add platform android
- Add Push-notifications plugin: cordova plugin add phonegap-plugin-push --variable SENDER_ID="XXXXXXX"
- Add phonegap plugin code in the app: https://github.com/phonegap/phonegap-plugin-push/blob/master/docs/EXAMPLES.md


#questions: 

- Send picture 
- Test when the app is closed 
- Try to test it with web app or chrome app 

# Triggers: 

//  Event.status = created then send email/sms/push notification to Attendee
//  If it is not accepted before 24 hrs then send another notification
//  If it is not accepted before 12 hrs then send another notification

// Event_created

// Event_scheduled
// Event_re_scheduled
//   Event_start_changed
//   Event_end_changed
// Event_attendee_changed
// Event_location_changed
// New_message
// Reply_message
// User

