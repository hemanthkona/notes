# Best Practices

### CSS
  - CSSLint
  - Trello Styleguide [https://gist.github.com/bobbygrace/9e961e8982f42eb91b80]
  - AirBnb Styleguide [https://github.com/airbnb/css]

### JavaScript

  - ESLint | TSLint | Codelyzer
  - AirBnb Styleguide [https://github.com/airbnb/javascript]
  - Angular 2 Styleguide [https://angular.io/styleguide

### Git

  - git-extras [https://github.com/tj/git-extras]

## Tools

### IDE (Atom)

  - Emmet
  - ESLint
  - Typescript-atom
  - atom-tslint
  - angular-2-typescript-snippets
  - https://gist.github.com/HemanthKona/5aabdc9b7f737f7a41e1

### Terminal Tools

  - Terminator
