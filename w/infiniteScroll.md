# Infinite Scroll

Lazy load rows in the UI grid, create DOM for row in the Viewport

# GOAL

- Speed grid load time
-  


# Tasks

- Add infinite scroll attribute on html
- Add infinite scroll service to the module
- Configure infinite scroll
