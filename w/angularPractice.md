# Angular Best Practices

- Avoid function expressionns
```javascript
var sampleFunc = function() {
    // logic
}
```
- Use named function
```javascript
function sampleFunc() {
    // logic
}
```
