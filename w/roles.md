# Specs: User Role Permissions

    - /Hide statistics card
    - /Add description to schema
    - /overlay: show color: label in the same field
    - /Remove delete icon from edit page, add it to overlay
    - /Go through the edit template and add missing schema
    - /Lock the role type after creating, only editable when creating new role
    - /Switching roles: should only hide modules
    - Fix css

    - /Audit the system to find all permissions: make sure permissions are used appropriately  
    - /Audit no role id hard coded instead use role_type
    - Account level permissions: show only relevant permissions for that account/ tier
        - In schema: create hidden modules which no one can see; incomplete/in dev mode  
        - User.js: merge permissions
        - also check account level permissions as well before going to a module
    - user override permissions (use an empty placeholder object for now)

    - Set up good defaults
    - Reset to defaults, security modals: are sure to reset

## User

    - /Hide Manager module permissions
    - /user.js: check the role type for manager modules

## Manger

    - /Don't allow them to change role type to user
    - /Change label to Back office
    - /Executive will be Back office

## Executive

    - /Should not edit / delete this role
    - /Should not show up in user management.info roles  as an option
    - /Should be able to assign new user or change executive user
    - Move stat holiday multiplier to visit settings card
    - Only Executive should be able to edit Visit settings card
    - /accounts.notifications(in dev mode): Only executives should see

    - /remove edit button from the overlay
    - /shouldn't be able to delete/suspend executive user,
    - /show executive label on executive user
    - /user.info.edit: lock role field
    - /add new card on the edit page to change executive user
    - accounts.settings, accounts.roles should only be editable by executive

## Create new role

    - /Start with: everything false, default: none

## validate

    - cannot edit the role type except if its new
    - Should validate that you can't edit the executive role

## Automate tests

## Future

    - Automatically generate template for editing permissions based on schema

## Proposals

    - Create roles collections
    - setup cross-account documents to serve as defaults (with unique database_type flag for each)
    - on create new account or role, copy these defaults
    - setup back-end function/hot-fix to insert new permissions
        - pass permission schema (patients.permissions.submodule.permissions.name) & label
        - update default roles, dev mode excludes, account tiers, labels, all roles in roles collection separated by user/manager role type
