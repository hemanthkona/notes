Run custom bash scripts from anywhere in the terminal

Hello everyone, Today I m going to show you an easy way to run custom bash commands from any where in the terminal, regardless of which folder you are in, you can run bash commands.

One way I used to run my own bash scripts is by adding the folder in which bash scripts exists to the path variable, which is quite common.

Suppose I have my bash scripts in
```shell
    home/Documents/scripts/
```
then I would add it path variable so that I can execute my commands from anywhere.

If you want to access this commands after rebooting you need to add it to .profile or .bash_profile

I was trying to do the same but fortunately I saw my .profile had this script where it looks for a bin directory in my home directoy. If it finds that bin directory then it add the ~/home/bin directory the path that means we can just create a bash file in the bin directory and add execute perission then you are done.

Let say you have scripts in some other directories,

Documents/scripts/main
you can just add a symbolic link in the bin folder to make it executable from anyehere in the terminal.

This simple solution helped me organize my bash scripts. Hope you find it useful. Thanks
