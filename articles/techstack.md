# Tech Stack

## Essential - Front End Technologies

Sorted in the order that make sense to learn

###### Links

- http://flexboxfroggy.com/
- http://egghead.io ***Tldr; One place for all things javascript, start with this site to understand the concepts in short amount of time. Most of the tech I mentioned below can be found in this website.***

#### ESNext

  - String Interpolation
    - Ex: using back tics for writing multiple lines and concatinating string
```javascript
    let concatString = `Concatenate string using ${variableName} and also write
    multiple lines use ful for html templates
    <span> Some Markup </span>
    You get the point`
```
  - Array.reduce, .map, .filter
  - Object.assign, Object.create
  - Collections: Map, WeakMap, Set, WeakSet ..
  - Arrow Functions
  - Generator Functions, Iterators

```javascript
function *generatorFunc() {
  let result1 = "return first value and stop executing the function";
  let result2 = "return second value when requested"
  yeild result1; // returns result1 and pause execiteing the function after this line

  yeild result2; // return result2 when called by generator.next function
}  

let response = generatorFunc();
response.next(); // {value: "return first value ..  ", done: false};
response.next() // {value: "return second value ...", done: true}
```
  - Async Functions
    - (Ex:
```javascript
      async function backendCall() {
        let firstPromise = await new Promise();
        let secondPromise = await new Promise();

        let response  = firstPromise + secondPromise;  
        console.log(response);
      })
```
  - Decorators
    - Used to provide meta data, essestially a function under the hood
    - Ex:

```javascript
// Decorator
@Component({
  name: "App",
  ...
})
class AppComponent {

}
```
  - Module System

#### Web Components

  - Custom Elements
  - Templates
  - Shadow Dom
  - :host - isolated styling

#### CSS

  - Flexbox
  - new pseudo selectors- :root, :host ..
  - css variables
  - calc() methods ...
  - Grid Layout
  - Sass/Stylus/PostCSS

#### Functional Programming

#### Reactive Programming

- RxJS: (Observable, observer, subscribe)
- Hot observables, cold observables
- Destroying obserables, memory clean
- RxJS Operators (map, filter, flatmap, counter,
  ...)
- RxMarbles
- Task scheduling and queues on client side

#### Typescript

  - Data Types
  - Interfaces
  - Async Await
  - Typings (.tds) https://github.com/DefinitelyTyped/DefinitelyTyped

#### Node.js and NPM

  - Basic understdaning of node modules
  - Using Node Package Manager
  - Semantic Versioning
  - Contents of Package.json
  - Write Build scripts  

#### Build System

  - Webpack
  - Webpack-dev-server
    - Live reloading
    - Hot Module reloading (replace code in the browser )

#### Angular 2

  - Components
  - Services
  - Filters
  - Data binding syntax
  - Dependency Injection
  - inputs(properties) (parent -> child) /
  - outputs(events) (child -> parent(s))
  - component Routing
  - Server Side rendering

---

## Recommended

#### Progressive Web Apps

  - Service Workers
  - Offline web experience
  - Web Workers
  - Local Storage
  - Web Push Notifications

#### Immutable Data
*** Single source of truth, predictable state management for large applications ***

  - Flux
  - Redux
  - ngRx(rx-store)

#### Testing Tools

  - Protractor - Measure Angles(AngularJs)
    - Unit Testing
    - E2E Testing
  - Regression Testing
    - PhantomJs
    - PhantomCSS
  - UI Testing(Sauce Labs)
  - browsersync

### Best Practices

  - Angular Style Guide
  - Airbnb Style Guide

#### IDE

  - Atom, Sublime
  - Typescript
  - EditorConfig
  - Linting

#### Continuous Interation

  - Code Linting
  - Code quality
  - Cloud Testing

#### UI Framework

  - http://getuikit.com/
  - https://getmdl.io/components
  - http://www.primefaces.org/primeng/#/
  - http://fuelinteractive.github.io/fuel-ui/#/
  - http://ng-lightning.github.io/ng-lightning/#/
  - http://semantic-ui.com/
  - https://elements.polymer-project.org
  - https://customelements.io/

---

## Extras

- Generate Templates/Code generate https://www.npmjs.com/package/plop

#### Algorithms Used by TypeScript, Webpack for compiling and optimizing

- AST => for compiling ES6 to ES5
- Tree Shaking => minifying and removing dead code

#### API / Backend

- Graphql
- Falcor
- Authentication (JWT, OAuth)

#### UI Libraries

- http://scrollmagic.io/
- https://scrollrevealjs.org/

### Advanced

- https://leanpub.com/understandinges6/read
- https://egghead.io/courses/mastering-asynchronous-programming-the-end-of-the-loop
- http://horizon.io/api/collection/
