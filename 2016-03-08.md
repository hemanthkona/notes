# All about Aws

## Install CLI

    - Install Python@2.7
    - Install pip
    - > pip install --upgrade awscli

## Check aws installation

    - > aws --version
        aws-cli/1.10.10 Python/2.7.6 Linux/3.13.0-79-generic botocore/1.4.1

## Get access key and secret key from AWS Console

access key and secret key are used to get access to aws cloud using command line

    - Login aws Console
    - goto: IAM (Identity and Access Management)
    - group: create a group(Ex: AL-ADMIN), with below policies
        - policies:
            - AdministratorAccess
            - AmazonEC2FullAccess
            - AmazonEC2ContainerServiceforEC2Role
    - create user and add to AL-ADMIN group
        - create access key
            - copy the generated 'access key' & 'secret key'
            - you can view secret key only once, at the time of creating a access key. So make sure to copy and keep it in a safe place
            - If you loose secret key, delete the access key and repeat [create access key]   

## Configure CLI

using the access key and secret key

    - > aws configure
        - enter the respective keys when prompted
            - access key: [your access key]
            - secret key: [your secret key]
            - output format: json | table | text
            - region: us-east-1

    - create different configuration settings
        - > aws configure --profile prod
            - create production configuration
        - > aws configure set | get profile.prod.region us-east-1

## Importand Locations

configuration location:  ~/.aws/config : only used by cli
credentials location: ~/.aws/credentials: used by all aws sdks

## convention

Anything that is created using command line will be prepended with 'al-cli'
Ex: aws ecs create-cluster --cluster-name "al-cli-cluster"

## create user group

    - > aws iam create-group --group "al-cli-user-group"

## create cluster

aws ecs create-cluster help
aws ecs --generate-cli-skeleton

    - > aws ecs list-clusters : list available clusters  
    - > aws ecs create-cluster --cluster-name "al-cli-cluster"
    - > aws ecs delete-cluster --cluster "arn:aws:ecs:us-east-1:828928996713:cluster/al-cli-cluster"

## Launch Instances

    

## Commands

aws commands

    - > aws help: Display help document with all options and available services
    - > aws ecs help: Displays help document for ecs service with all available commands     
    - > aws ec2 describe-regions :
    - > aws ec2 describe-instances help : you can see help page for 'describe-instances'
    - > aws ec2 describe-instances --output table: use --output to print output in desired format(json|table|text)  

General

    - aws [service] help
    - aws [service] [action] help
    - aws [service] [action] --generate-cli-skeleton
    - aws [service] [action] --cli-input-json [{"key":"value"}]

## Progress

    - create key pair to ssh
    - create user group
        - add policies
    - create user
        - add to group
    - create role
        - add to group
    - create cluster

    - create security group
    - create elasticloadbalancer
    - create autoscaling launch configuration
    - create autoscaling group
    - Launch EC2 Instance
    - create a task in cluster
    - create a service
    - update a service
    - Blue green deployment
