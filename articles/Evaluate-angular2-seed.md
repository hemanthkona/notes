# Evaluate Angular2 seed projects

## Criteria
  - Ease of use
  - Extensible
  - Follows Best practices
  - Gentle Learning curve
  - Bleading edge tools, up to date dependencies
  - Good community support
  - Necessary config out of box, so that we dont have to tweak much

- official Angular2-seed [https://github.com/angular/angular2-seed]

  - Minimal Setup
  - Easy to get started
  - Less learning curve

  - Have to tweak a lot for our needs
  - We have to write lot of configuration code
  - No test setup

- mgechev/angular2-seed [https://github.com/mgechev/angular2-seed]

  - Test setup using protractor
  - client/server architecture built in
  - Well thought
  - Community Support
  - Electron and NativeScript support built in
  - Angularitics2 Built in
  - Uses angular2-rc-5: latest version
  - Support for native and desktop apps

  - Too advanced
  - complex setup, hard to make our own
  - Steep Learning curve
  - Uses typings instead of @types
  - Have few things which we might not use
  - Uses gulp build system instead of webpack

- AngularClass/angular2-webpack-starter [https://github.com/AngularClass/angular2-webpack-starter]

  - In sweet spot
  - Test setup using Protractor
  - Follows best practices
  - Uses angular2-rc-5: latest version
  - Uses @types instead of typings

  - Moderately complex setup
  - Reasonalble Learning curve

- Angular-CLI [https://cli.angular.io/]

  - In very early stages of development
  - Many features are disabled and buggy
  - We would want to incorporate cli tool once its atleast usable

- preboot/angular2-webpack [https://github.com/preboot/angular2-webpack]

  - Reasonably similary to AngularClass/angular2-webpack-starter
  - Moderate setup
  - Linting, compiling sass
  - Test setup using protractor
