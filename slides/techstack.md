# Tech Stack

What is ES5, ES6, ES2015, ES2016 ... ?

What is TypeScript ?
  - Why
  - How
  - Usage
  - Benefits
  - Alternates

What is Webpack ?

---

## Agenda

- TypeScript
- Webpack
- Different Module Systems
  - AMD
  - UMD
  - Javascript
- Project Architecture
-

---

##  Why we are using

  - To speed up development
  - To make less decisions ourself
  - To communicate better between the team  

---

## Doesn't our current stack do for now

---

## What we are using

  -Angular 2, Typescript, webpack

---

## Webpack
Module Bundler - Takes a entry file (app.js) and traverses throught the application using import, export and require statements and concatenates and minifies all the files into a bundle or creates as many files as we specify

  - Module Bundler
  - Simple configuration
  - Community
  - Currently the best tool for this functionality

---

## How is it Different than Gulp
Gulp is a task runner, it doesnt

## Angular 2

  - Smiliar to Angular 1 in terms of core concepts
  - Much of the heavy lifting has been done for large scale apps
  - Developed for Scalable and Mobile in mind
  - Community  

---

## TypeScript

  - Compiles code from latest version of javascript to preveious versions
  - Great developer experience once you get the hang of it
  - Tooling for IDE's
  - Community

---

## FlexboxGrid

  - Easy to use
  - Most comphrehensive
  - Fall backs to Box Model, if flexbox is not supported
  - Stable project
  - Naming conventions compatible with bootstrap
  - Easy to learn
  - We dont have to write media queries for various device sizes

---

## Mobile first Design

  - Design UI for mobile device size first
  - Which will automatically scale for desktop with slight modifications
