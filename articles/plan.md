# Picture

## Design

  - Overall
    - Resolves
    - Lazy Loading
    - Pre Fetching
    - Service Worker Caching
    - Smooth Transition
  - Messaing Component
    - Messages Lists
    - Messages Thread
    - Notification
    - Online Status
  - Visits
    - Booking
    - Edit Booking
  - View Booking
      - Three sections
        - Active
        - Finished
        -
    -
  - Caregivers
    - Search
      - Search for some criteria
      - Show list of

## Plan

## Components
  - Select: `2 Days`
  - Select Only One(Radio): `2 Days`
  - Select Multiple(checkboxes): `4 Days`
  - Date picker: `1 Week`
  - Modal Pickers: `1 Week`
  - Validation: `2 Days`
  - Loading Screen Component: `2 Days`
  - Backend Error Code Handling: `2 Days`
  - Security and REST API Component: `1 Week`

## Modules

### Menus

  - Actions Menu: `2 Days`
  - Main Menu : `1 Days`
  - Sub Menu inside a module : `1 Days`

### REST APi

  - CRUD API for Caregiver: `2 Weeks`
    - Create new caregiver
      - take all data form front-end and save to database and send an email
      - Validate all data and send errors to front end
  - CRUD API for Client: `2 Weeks`
  - Validation in the backend: `1 Week`
  - Search API: `2 Weeks`

### Care Giver

  - Profile View: `4 Days`
  - Edit View: `1 Week`
  - Show all Clients associated with Caregiver

### Client

  - Profile view: `4 Days`
  - Edit View: `1 Week`
  - Show all caregivers associated with clients

### Search Module

  - Search Box : `1 Day`
  - Search view: `2 Days`
  - Actions on search results: `2 Days`

### Scheduling

  - Create Event: `1 Week`
  - Edit Event: `1 Week`
  - View Event: `2 Days`

## Security : `2 Weeks`

## Testing: `2 Weeks`

## Bug fixes: `2 Weeks`

## Code Reafactor: `1 Week`

## Grace Period: `2 Weeks`
