# Weekly Updates

May 8th, 2017 - May12th, 2017

# Accomplished

- Implemented Blacklisting and Favouriting Users
- Improve the design of welcome email, align the features in one row and use consistent fonts
- Add confirm account link in the welcome email
- Add "promo_code" field on client intake form
- Setup unit test framework using Node.js, Mocha, Chai libraries
- Implemented unit tests for testing Stripe Customer, Account Classes
- Fixed issue with caregiver intake form pin number validation bug after saving the pin to database
- Fixed ConstructSignature function to not use null values

# Next Steps
- Create Carepros component on admin site
- Complete Whitelabeling Email templates
- Write chron job to charge customers after completing of visit
- Test Stripe Charge, Transfer, Balance Classes
- Write end-end tests to test stripe backend and front-end integration


Apirl 22nd, 2017 - April 26th, 2017

# Accomplished

- Implemented empty states on Caregiver Grid, Client Grid, Visits Grid, Conversations Grid views
- Replaced all form arrays with new abstracted FormArray Service
- Implemented ability to search for location and add location manually using location form
- Fixed issue with cirucular dependency using webpack
- Fixed issue with uploading photograph on caregiver and client intake form


# Next Steps

- Implement Favourite and Unfavourite caregivers
- Implement blacklisting caregivers and clients
- Fix bugs on repeating location search and location form
- Test Stripe Api wrapper library, unit tests and regression tests
- Write Cron job to charge customer after completing the visit

April 17th, 2017 - April 21st, 2017

# Accomplished

- Ugrage Angular Version from 2.2 to 4.0.2
- Upgrade Ngrx store to version 2.2 and refactor the code to make it compatible with Angualr 4.0.2
- Upgrade TypeScript, Webpack, TsLint and Update code to make it complatible with new version of TypeScript and TsLint
- Refactor Angular Forms to support adding, removing, editing Multiple Phones, Location, Languages, Services
- Rafactor and Abstract FormArray Operation and Validations  
- Update Sentry Logger to catch
- Fix RangeError caused when uploading Photographs
-

# Next Steps

- Replace all form arrays in the system to use new abstracted FormArray service
- Write a Cron job to charge customer after completing a visit using Stripe api
- Update credit card and banking information intake on front-end
- Update ability to select credit card when creating a visit
- Fix issue: TypeError: Object doesn't support this action
- Fix issue: Unable to get property '0' of undefined or null reference

April 10th, 2017 - April 14th, 2017

# Accomplished

- Implemented filtering caregivers based on services and patients
- Implemented client progress-notes & conditions
- Implemented Risk Assessment, Form1 forms
- Implemented live calendar availability on caregiver profile

# Next Steps

- Fix Angular 2 Forms bug with adding multiple locations, services, languages, phones
- Abstract form array component, validation and template code
- Fix edit visit bug, regarding service pay_rate.rate, _id is undefined
- Upgrade Angular Dependencies, Upgrage Ngrx dependencies
- Test & Finalise stripe integration  

Mar 20th, 2017 - Mar 24th, 2017

# Accomplished

- Implemented visit check-in using geoposition system  
- Updated intake form with new design
- Updated welcome email template
- Implemented sign-up form for Brain Injury Service

# Next Steps

- Finalize intake form re-designs
- Add pin field for caregiver
- Implement Ratings and Reviews for caregiver
- Implement multiple locations, multiple languages on intake form
- Design Login page

Mar 6th, 2017 - Mar 10th, 2017

# Accomplished

- Implemented sending push notification to Android device
- Updated welcome email user interface to use new template design
- Implemented white labeling front-end and back-end code
- Implemented white labeling email templates
- Implemented OT assessment form

# Next Steps

- Design Application layout
- Design create new visit view
- Design Credit card and banking intake forms  
- Design cards to show relevant information on caregivers, visits grid pages
- Design caregiver filter pop up
- Design profile details view    

Feb 27th, 2017 - Mar 3rd, 2017

# Accomplished

- Researched UI component designs
- Implemented wrapper function to send email notifications using AWS Ses Service
- Implemented wrapper function to send sms notifications using AWS Sns service
- Implemented UI to create custom notifications on the user side
- Setup mobile app to test push notifications

# Next Steps

- Implement sending push notification to mobile devices
- Come up with predefined triggers to send notifications
- Implement triggers and actions on the backend to send notifications
- Implement chron job to send notifications
- Implement UI for users to choose notification destination
- Implement UI for managers to toggle notifications status

Feb 20th, 2017 - Feb 24th, 2017

# Accomplished

- Implemented preventing creating visit unless caregiver has proper bank details
- Implemented the stripe webhook to take action on stripe events
- Implemented charging a customer using stripe credit card in the backend
- Fixed link in the welcome email template

# Next Steps

- Email remainders and notifications
	- twillio and amazon anyother
	- can we shred
	- hyposthesis : is it possible to send notifications
	- uncertain:
	- create library
	- create option in the databse as a setting
	- build it into the front-end, give an option to
- Research and design app layout
- Implement navigating views by swiping left or right
- Design create new visit view
- Design cards to show relevant information on caregivers, visits grid pages
- Design caregiver filter pop up
- Design profile details view    

Feb 13th, 2017 - Feb 17th, 2017

# Accomplished

Implemented adding repeating services on caregiver intake form
Implemented edit repeating services on caregiver edit page
Implemented filter by service on caregiver grid list
Implemented error reporting on the backend to capture stripe errors
Fixed issue: select component not updating on adding a new credit card on visit create page
Fixed issue: sending multiple subdocuments on editing intake form
Fixed issue: unable to complete registration if clicked back and returned to last page
Fixed issue: if default values are set, default values are not saved to database

# Next Steps

Implement stripe billing rules on the backend
Implement preventing creating a visit unless caregiver has proper bank details
Implement stripe webhooks: account.updated hook to listen for account updates
Implement avoding fraud using Stripe Radar rules  

Feb 6th, 2017 - Feb 10th, 2017

# Accomlished

Implemented creating charges on stripe account and collect fees
Implemented adding multiple credit and bank accounts
Implemented selecting credit card component on visit create view
Implemented adding new credit card on visit create view
Implemented switching primary credit and bank account

# Next Steps

Implement charging client credit card on event complete
Implement exception logging on the backend for capturing errors with stripe api
Test stripe api for all edge cases with different data sets
Implement adding repeating services and rates
Fix issue: select component not updating on adding a new credit card on visit create page
Fix critical bugs on intake forms
 - fix sending multiple subdocuments
 - properly merge subdocuments returned from backend
 - Unable to complete registration if clicked back and retunred to last step
 - if default is set, default values are not saved to database

Jan 30th, 2017 - Feb 3rd, 2016

# Accomplished

Implemented creating stripe managed account for caregivers
Implemented creating stripe customer account for clients
Implemented retriving stripe account from stripe database
Implemented retriving stripe customer account from stripe database
Implemented securely sending credit card info and store in stripe database
Implemented securely sending banking info and store in stripe database

# Next Steps

Implement creating charges on stripe account
Implement adding multiple credit cards to the stripe account
Implement adding multiple banking accounts to the stripe account
Implement switching primary credit card
Implement switching primary banking account
Design and improve the user interface of the application

Jan 23rd, 2017 - Jan 27th, 2017

# Accomplished

- Implemented and finalised resource cache
- Implemented toggle switch buttons instead of checkboxes
- Integrate form into wordpress website
- Update user, visit models, skills, picklist models
- Fix caregiver filltering  thought the search input doesn't change
- Fix location component to convert state as string to id

# Next Steps

- Update welcome email templates design and content
- Implement analytics and reporting
- Implement billing component

Jan 16th, 2017- Jan 18th, 2017

#Accomplished

- Implemented filtering caregiver list by name
- Implemented creating user from bloom wordpress site
- Implemented Resource Cache to store retrived data and reduce backend requests
- Implemented storing photos in the resource cache and optimised loading photos on subsequent request
- Fixed issue with using loggedInuser object instead of query params from object
- Fixed issue on conversations list filter by message
- Fixed issue with requesting favicons on every state change
- Fixed issue with relation value not showing on the input field

# Next Steps

- Fix issues on bloom wordpress site: careate using and processing response
- Fix validation css issue on bloom wordpress site
- Optimise retriving photos on Visits Grid, Conversations module, Event Invitaion Card component
- Implement analytics and reporting tools on the frontend

Jan 9th, 2017 - Jan 13th, 2017

# Accomplished

- Implemented rate validator
- Implemented Visits grid pagination
- Implemented main context menu on header bar instead of menu bar
- Implemented toggle switch button and replace checkbox buttons
- Implemented a checkmark at the bottom of the skills toggle checkbox
- Implemented search by message feature on messages list view
- Optimised getting photos on caregiver grid view
- Improvement: Sort messages list by last message sent date instead of created date
- Fixed styles: caregiver, client details page for mobile screen
- Fixed img out of place on messages list on clicking load more button
- Fixed issue with scrolling on visits grid when there are more than 10 visits
- Fixed back button not working on profile-edit page
- Fixed issue with unselecting skills after saving on intake form
- Fixed mobiscroll not initalising search form on caregiver search
- Fixed sending invitation message on re-scheduling visit
- Fixed issue with showing re-schedule button on invitation card for scheduled visits

# Next Steps

- Implement search by name on caregivers list, visits list view
- Integrate signup forms into wordpress landing site
- Implement resource cache using redux store for improving performance of the app
- Implement pusher on the front-end and integrate with resource cache
- Implement History service to keep track of state and improve the correctness of back button

Jan 4th, 2017 - Jan 6th 2017

# Accomplished

- Upgraded Angular, Webpack and Typescript libraries
- Implemented Automatic Error Reporting
- Fixed issue with generating sourcemaps for production builds
- Optimized getting photos asynchronously on messages module

# Next Steps

- Fix issue with location search component
- Fix issue with back button on the messages module
- Integrating bloom app with wordpress site
- Implmenting necessary securty for the application
- Implementing analytics and reporting tools
- Update intake form User interface design  
