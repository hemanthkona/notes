# Editor Extensions List
https://gist.github.com/HemanthKona/5aabdc9b7f737f7a41e1

## Atom

### Essential

  - atom-typescript
  - Wakatime
  - editorconfig

  - linter
  - linter-tslint
  - atom-beautify

  - autocomplete-plus
  - autocomplete-css
  - autocomplete-html
  - autocomplete-modules
  - autocomplete-snippets
  - atom-ternjs - Javascript api autocomplete

  - emmet

### Recommended

  - Pigments
  - sync-settings

### Extras

  - File icons
  - minimap
  -

## Sublime

  - git

## Commandline tools

  - git extras
  - Node.js
