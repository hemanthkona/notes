# Scheduler

    - update users and patients cache when pusher updates resources

## Target

    - Performance
    - User friendly
    - Modular
    - Future friendly
    - Write once

## Learn

    - ng-upgrade
    - fullcalendar api
    - rxjs

## Topics

    - Architecture
    - Manage resources(Users/Patients)
    - Build Query/ Back-end call
    - Filtering (filter overlay/ search bar)
    - Paginating resources,
    - Caching Strategy
        - Service worker
        - Network first strategy
    - Real-time updates

## Scheduler time line

    - Write modular so that framework doesnt matter
    - Subscribe to resource cache using rxjs

    - Cache static/assets

    - fast loading
    - timeline views
    - schedule & grid views sharing underlying model
    - highlighting cols/rows/headers
    - scheduler positioning settings
    - mobile strategy
    - live updates
    - tabbed views when opening other modules
    - event block coloring
    - arrows to indicate hidden data
    - print schedule


## Event block

    - Show start, end ,attendee, show status as background color
    - get tooltip data onclick and Render tooltip on fly
    - Ability to drag and drop events and edit events  


## Backend

    - Create event block html
    - create qtip tooltip html
    - On event-drop: show a toast saying that the changes to the event are in progress
    - Show toast error with relevant info and revert the changes in the front-end
    - Hide tooltip onscroll, or pin it to event
    -

- patient changed
{
    patient: Objectid
}

- attendee change
{
    attendees: {
        {
          _id: objectid,
          user: objectid
        }
    }
}


- should we allow them to drag events to time in the past ?
- If there is any other event for that attendee/patient at the same timings ?
