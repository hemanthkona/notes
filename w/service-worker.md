# Service Worker

Cache assets and server them off-line

---

## Web Worker

Web Workers: Bring Threading to JavaScript

    - Concurrently execute multiple scripts
    - Process large amount of API data
    - Run scripts in the background
        - can be useful to recieve pusher data
    - Non-blocking, multi-threaded
    - Cannot access dom
    - So other than dom manipulations, all the heavy json data processing

---

## Caching HTML/CSS/JS

## Offline app

## Background-sync

## Push Notifications
