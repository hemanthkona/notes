# Auto/Prompt front-end Update

- rollout.io - ruled-out
    - only works for native IOS apps
    - currently have support only for Objective C
- ionic.io
    - Still in beta
    - Supports IOS, android
    - Doesn't support browser or web updates

- https://github.com/torrmal/cordova-standalone-hydration
    - Only works for cordova platform
- http://stackoverflow.com/questions/15880866/phonegap-android-app-trigger-update-programmatically
    -
- cdn using versioning
    -
- aws versioning and download new files

- Using service workers
    - https://serviceworke.rs/fetching.html
    - https://jakearchibald.com/2014/offline-cookbook/
    - http://www.html5rocks.com/en/tutorials/service-worker/introduction/

- show popup to users, when they click get updated resources

## Requirements

- show refresh banner when we push new updates
- able to update mobile app on the fly, whitout going thought the appstore
- Automatically download all new udpates without clearing the cache cmanually

## update

- update HTML, CSS, JS

- load files on the device
    - cannot update the files on the fly
- load files from server
    - cannot do platform/device specific things

## Decision Criteria

- speed of development
- cross platform, mobile, web, native
- App store safe
- ease of update


## Proposals

- Use caching
    - Platform agnostic, works on http caching mechanism

- Use Etag to determine whether the html/css/js file is updated or not
- It can be configured at the server(nginx) level, using http headers
    - cache-control: 120 seconds // amount of time to cache file
    - ETag: 'xcvxw9' // hash to determine whether the contents of the file changed

- We have the control, cache invalidation ....
- We can persist the cache on the mobile device ( not sure how complicated it is ?)

- Doesn't need any configuration json files
- Doesn't need manual updating
- Nothing to change on the front-end, browser handles invalidating cache using the ETag hash

- https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/http-caching?hl=en
- https://github.com/h5bp/server-configs-nginx
- https://developers.facebook.com/docs/marketing-api/etags
- http://stackoverflow.com/questions/499966/etag-vs-header-expires

### Mobile

- Store files on the server instead of device for the above mentioned reasons
- Use pusher, register pusher on page load to watch for update event
    - show update/refresh banner

## Problems solved

- Mobile: Updating app
- No need for hard refresh in the browser

## To find solution

- Caching files on the device and persisting, Expecting to load from cache in the browser
-

## Questions

- How does rollback to previous update work ?
- How to find whether we are on mobile or desktop ? mobilePhonegap ?
- Does hosting JavaScript files on a CDN, will create any anomaly with our app ?
    - Having diffrent versions of JavaScript and HTML at the same time.

## Proof of Concept

- Update s3 bucket with updated JavaScript files  


## Conclusion

- Don't store files on the device
    - Cannot update app on the fly
- Get Everything from Server
    - Can update on the fly

- Use Caching
- Use Service worker for caching and Off-line app


## Future Improvements

### -Responsive- Progressive Web App

- https://developers.google.com/web/progressive-web-apps#getstarted
- http://www.html5rocks.com/en/tutorials/service-worker/introduction/
- https://developers.google.com/web/updates/2015/11/app-shell?hl=en
-

## Questions

- Does aws s3 has a load balancer for regions ?
- Browser Analytics ?


## Testing

- Caching: Make sure you are getting 304 for unmodified files
- Updating to new Files: If you update any js/css files, browser should request for new files

export ANDROID_HOME=/home/dquintana/Android/Sdk
export PATH=$PATH:/home/dquintana/Android/Sdk/tools


### Web

- app is open => show a refresh button via pusher
- closed an reopen => 

### Mobile
- app is open  => show a refresh button
- closed an reopen = > getting from cache / but should check backend
-  
