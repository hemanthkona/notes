# REST

### Patient Sign up
  - create a open end point similar to Healthnote-candidates which can be accessed with out authentication
  - Recieve the post data from front-end
  - Shoot email(s) using the post data
    - For time being just send to 'andrew.ringer@alaunus.com'

POST api.com/signup-patient
POST api.com/signup-caregiver

payload for /signup-patient:

{
  recipient: '',
  more_recipients: '',
  location: '',
  start_date: '',
  care_needs: '',
  preferred_gender: '',
  allday_care: '',
  language: '',
  duration: '',
  email: '',
  phone: '',
  address: {
    street: '',
    unit: '',
    city: '',
    state: '',
    conutry: ''
  },
  caretype: {
    bathing_assistance: false,
    bathroom_assistance: false,
    companionship: false,
    dressing: false,
    transitional_care: false,
    houe_keeping: false,
    meal_preparation: false,
    medication_management: false,
    post_op_recovery: false,
    transportaion: false
  },
  schedule: {
    monday: {
      morning: false,
      afternoon: false,
      evening: false
    },
    tuesday: {
      morning: false,
      afternoon: false,
      evening: false
    },
    wednesday: {
      morning: false,
      afternoon: false,
      evening: false
    },
    thursday: {
      morning: false,
      afternoon: false,
      evening: false
    },
    friday: {
      morning: false,
      afternoon: false,
      evening: false
    },
    saturday: {
      morning: false,
      afternoon: false,
      evening: false
    },
    sunday: {
      morning: false,
      afternoon: false,
      evening: false
    }
  }
};
