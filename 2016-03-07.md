# Keep things simple

## Good

    - Arrived at work on time
    - Openly expressing whats in mind
    - Keeping head up

## Bad

    - Not bringing lunch
    - Eating Pizza
    - Pay little more attention to detail 
