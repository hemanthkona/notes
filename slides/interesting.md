# Sharing is Caring, Concepts

---

## Functional Programming

    - Way of writing functions
    - Pure functions
    - same input should produce same output always
    - Should not modify the original object

### Benefits

    - output is predictable
    - easy to test
    - Framework Agnostic

---

## Reactive Programming

    - Observable type
    - Observer pattern/pub-sub patttern
    - https://reactivex.io/
    - cycle.js
    - egghead.io
    - github reactive programming introduction

---

## Atom

    -  Syn Settings: gist.github.com
    -  File Icons

---

### Why Atom

    - Open Source
    - Built by Github
    - Built on chromium and Nodejs
    - Hackable to the core
    - Close to same functionality as Sublime
    - Cool Plugins
    - Gaining traction in the community
    - Easy to config or change color
    - http://apiworkbench.com/ for api developing and testing

---

### Technologies Behind Atom

    - Chromium
    - Node.Js
    - Electron
    - React
    - Web Components
        - Custom elements
        - Shadow Dom

---

### Cutomize Atom

    - Default settings
        - Tabs, fonts, ignore,
    - Tree view
        - ignore files
    - Pigments
        - Show bckground color, dot color,
        - Show color pallet for project

---

## Git

    - git aliases
    - git revert

---

## Testing frameworks

    - https://mochajs.org/
    - http://chaijs.com/
    - http://jasmine.github.io/2.4/introduction.html
    - http://wallabyjs.com/

---

## API Specs

    - http://apiworkbench.com/, RAML
    - Swagger api

---

## Convention

    - request, response
    - make use of map, filter, foreach functions
        - immutable data, returns new collection

---

## TypeScript
